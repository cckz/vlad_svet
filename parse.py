# -*- coding: utf-8 -*-
import os
from grab import Grab
from urllib.request import urlretrieve

from django.core.files import File

from catalog.models import categoryModel
from catalog.models import categoryClassModel

def parseLed():
    grabObject = Grab()
    grabObject.go('http://ledpremium.ru/')

    if grabObject.xpath_text('//title') == '500' or grabObject.xpath_text('//title') == '404':
        print('false 500 or 404')
        return False

    for itemCategory in grabObject.doc.select('//ul[@class="general-menu"] //li'):
        try:
            nameCategory = itemCategory.select('.//a/span').text()
            imageCategorySrc = itemCategory.select('.//a/img').attr('src')
        except IndexError:
            pass

        if nameCategory:
            try:
                categoryModel.objects.get(name=nameCategory)
                continue
            except categoryModel.DoesNotExist:
                newCategory = categoryModel()
                newCategory.name = nameCategory
                absoluteUrlImg = grabObject.make_url_absolute(imageCategorySrc)
                filename = os.path.basename(absoluteUrlImg)
                img_temp = urlretrieve(absoluteUrlImg)[0]
                with open(img_temp, 'rb') as fupload:
                    newCategory.image.save(filename, File(fupload))

                if itemCategory.select('.//table/tbody/tr/td/div[@class="div_line"]'):
                    print(nameCategory)
                for subcategory in itemCategory.select('.//table/tbody/tr/td/div[@class="div_line"]'):
                      subname = subcategory.select('.//a').text()
                      if subname:
                            newSubCategory = categoryClassModel()
                            newSubCategory.name = subname
                            newSubCategory.categoriesClass = newCategory
                            with open(img_temp, 'rb') as fupload:
                                newSubCategory.image.save(filename, File(fupload))

        if nameCategory == u'Светодиодные рекламные доски':
            break

parseLed()
