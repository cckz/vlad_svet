# -*- coding: utf-8 -*-
from account.models import profileUser

def retrieve(request):
    """Возвразает экземпляр класса форма профиля пользователя"""
    try:
        profile = request.user.profileuser
    # если у пользователя не было профиля, то создаем его
    except profileUser.DoesNotExist:
        profile = profileUser(toUser=request.user)
        profile.save()
    return profile