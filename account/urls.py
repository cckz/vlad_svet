# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.contrib.auth.views import logout

from account.views import login
from account.views import register

urlpatterns = [
    url(r'^login/', login, name='login'),
    url(r'^register/', register, name='register'),
    url(r'^logout/', logout, {'next_page': '/'}, name='logout'),
]

