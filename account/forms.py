# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth import get_user_model
from django.contrib.auth import password_validation
from django.utils.text import capfirst

class authLoginForm(forms.ModelForm):
    error_messages = {
        'invalid_login': (
            u"Введите верный пароль"
        ),
        'inactive': (u"Пользователь не активен или не существует"),
    }

    class Meta:
        model = User
        fields = ('email',)

    password = forms.CharField(label=("Пароль"), strip=False, widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        super(authLoginForm, self).__init__(*args, **kwargs)
        self.fields['password'].widget.attrs['autocomplete'] = 'new-password'

    def clean(self):
        password = self.cleaned_data.get('password')
        email = self.cleaned_data.get('email')

        if email and password:
            try:
                username = User.objects.get(email=email).username
                self.user_cache = authenticate(username=username, password=password)
                if self.user_cache is None:
                    raise forms.ValidationError(
                        self.error_messages['invalid_login'],
                        code='invalid_login',
                    )
                else:
                    self.confirm_login_allowed(self.user_cache)
            except User.DoesNotExist:
                raise forms.ValidationError(
                    self.error_messages['inactive'],
                    code='inactive',
                )
        return self.cleaned_data

    def confirm_login_allowed(self, user):
        if not user.is_active:
            raise forms.ValidationError(
                self.error_messages['inactive'],
                code='inactive',
            )

    def get_user(self):
        return self.user_cache


class userCreationForm(forms.ModelForm):

    error_messages = {
        'password_mismatch': (u"Пароли не совпадают"),
        'emailExist': (u"Пользователь с указанной почтой уже зарегистрирован"),
        'emailNull': (u"Укажите электронный адрес"),
        'firstNull': (u"Укажите имя"),
        'lastNull': (u"Укажите фамилию"),
    }

    password1 = forms.CharField(label=(u"Пароль"), strip=False, widget=forms.PasswordInput)
    password2 = forms.CharField(label=(u"Подвердите пароль"), widget=forms.PasswordInput, strip=False,
                                help_text=(u"Enter the same password as before, for verification."))
    phone = forms.CharField(label=(u"Телефон"), strip=False, widget=forms.TextInput, required=False)

    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name',)

    def __init__(self, *args, **kwargs):
        super(userCreationForm, self).__init__(*args, **kwargs)
        self.fields['email'].required = True
        self.fields['email'].widget.attrs['required'] = 'required'
        self.fields['first_name'].required = True
        self.fields['first_name'].widget.attrs['required'] = 'required'
        self.fields['last_name'].required = True
        self.fields['last_name'].widget.attrs['required'] = 'required'
        self.fields['password1'].widget.attrs['autocomplete'] = 'new-password'
        self.fields['password2'].widget.attrs['autocomplete'] = 'new-password'

    def clean_email(self):
        email = self.cleaned_data.get(u'email')
        try:
            User.objects.get(email=email)
            raise forms.ValidationError(
                self.error_messages['emailExist'],
                code='email',
            )
        except User.DoesNotExist:
            if not email:
                raise forms.ValidationError(
                    self.error_messages['emailNull'],
                    code='email',
                )
            else:
                return email

    def clean_first_name(self):
        first_name = self.cleaned_data.get(u'first_name')
        if first_name:
            return first_name
        else:
            raise forms.ValidationError(
                self.error_messages['firstNull'],
                code='first',
            )

    def clean_last_name(self):
        last_name = self.cleaned_data.get(u'last_name')
        if last_name:
            return last_name
        else:
            raise forms.ValidationError(
                self.error_messages['lastNull'],
                code='last',
            )

    def clean_password2(self):
        password1 = self.cleaned_data.get(u"password1")
        password2 = self.cleaned_data.get(u"password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )

        self.instance.username = self.cleaned_data.get(u'username')
        password_validation.validate_password(self.cleaned_data.get(u'password2'), self.instance)
        return password2

    def save(self, commit=True):
        user = super(userCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data[u"password1"])

        if commit:
            user.save()
        return user