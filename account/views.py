# -*- coding: utf-8 -*-
import json
from django.http import HttpResponse
from django.http import HttpResponseBadRequest
from django.views.decorators.csrf import csrf_protect

from account.forms import authLoginForm
from account.forms import userCreationForm
from account import user_profile

@csrf_protect
def login(request):
    if request.method == 'POST' and request.is_ajax():
        postdata = request.POST.copy()
        form = authLoginForm(postdata)
        print(form)
        if form.is_valid():
            from django.contrib.auth import login
            login(request, form.get_user())
            return HttpResponse(status=200)
        else:
            errors_dict = {}
            if form.errors:
                for k, v in form.errors.items():
                    errors_dict[k] = v[0]
                    print(errors_dict)
                    return HttpResponseBadRequest(json.dumps(errors_dict, ensure_ascii=False).encode('utf-8'))
        print(form.errors)
    else:
        return HttpResponse(status=500)

@csrf_protect
def register(request):
    if request.method == 'POST' and request.is_ajax():
        postdata = request.POST.copy()
        form = userCreationForm(postdata)
        if form.is_valid():
            form.save()
            phone = form.cleaned_data[u'phone']
            un = form.cleaned_data[u'username']
            pw = form.cleaned_data[u'password1']
            from django.contrib.auth import login, authenticate
            new_user = authenticate(username=un, password=pw)
            if new_user and new_user.is_active:
                login(request, new_user)
                profile = user_profile.retrieve(request)
                if phone:
                    profile.phone = phone
                    profile.save()
                return HttpResponse(status=200)
        else:
            errors_dict = {}
            if form.errors:
                for k, v in form.errors.items():
                    errors_dict[k] = v[0]
                    print(k,v[0])
                    return HttpResponseBadRequest(json.dumps(errors_dict, ensure_ascii=False).encode('utf-8'))
    else:
        return HttpResponse(status=500)