# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User

class profileUser(models.Model):
    toUser = models.OneToOneField(User, verbose_name=u'Пользователь', unique=True)
    phone = models.CharField(verbose_name=u'Телефон', blank=True, max_length=10)

    class Meta:
        db_table = 'profile'
        verbose_name = u'Профиль пользователя'
        verbose_name_plural = u'Профиль пользователей'

    def __unicode__(self):
        return u'Пользователь #%s' % (self.toUser)

    def __str__(self):
        return u'Пользователь #%s' % (self.toUser)