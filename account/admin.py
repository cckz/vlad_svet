# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin as OriginalUserAdmin

from account.models import profileUser

class userProfileInline(admin.StackedInline):
    model = profileUser
    can_delete = False

class userAdmin(OriginalUserAdmin):
    list_display = ('username', 'get_full_name', 'email', 'get_phone', 'is_staff')
    inlines = [userProfileInline, ]

    def get_phone(self, obj):
        return obj.profileuser.phone
    get_phone.short_description = u'Телефон'

    def get_full_name(self, obj):
        full_name = '%s %s' % (obj.first_name, obj.last_name)
        return full_name.strip()
    get_full_name.short_description = u'Полное имя'

try:
    admin.site.unregister(User)
finally:
    admin.site.register(User, userAdmin)