# -*- coding: utf-8 -*-
from django.apps import AppConfig

class accountConfig(AppConfig):
    name = 'account'
    verbose_name = u'Пользователи'
