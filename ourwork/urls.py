# -*- coding: utf-8 -*-
import ourwork.views
from django.conf.urls import url

urlpatterns = [
    url(r'^$', ourwork.views.showAllWorks, {'template_name': 'ourworks/ourworks.html'},
        name='ourworks'),

    url(r'^(?P<ourwork_slug>[-\w]+)/$', ourwork.views.showOurwork,
        {'template_name': 'ourworks/ourwork.html'}, name='ourwork'),
]

