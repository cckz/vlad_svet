# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.core.paginator import EmptyPage
from django.core.paginator import Paginator
from django.core.paginator import PageNotAnInteger

from ourwork.models import ourWorksModel

def showAllWorks(request, template_name):
    allWorks = ourWorksModel.objects.filter(isActive=True)
    page_title = u'Наши работы'

    paginator = Paginator(allWorks, 12)
    page = request.GET.get('page')
    try:
        works = paginator.page(page)
    except PageNotAnInteger:
        works = paginator.page(1)
    except EmptyPage:
        works = paginator.page(paginator.num_pages)
    return render(request, template_name, locals())

def showOurwork(request, ourwork_slug, template_name):
    work = get_object_or_404(ourWorksModel, slug=ourwork_slug)
    page_title = work.name
    return render(request, template_name, locals())
