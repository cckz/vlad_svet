# -*- coding: utf-8 -*-
from django.db import models
from autoslug import AutoSlugField
from tinymce.models import HTMLField

class ourWorksModel(models.Model):
    name = models.CharField(max_length=250, unique=True, verbose_name=u"Заголовок")
    slug = AutoSlugField(populate_from='name', unique=True)
    description = HTMLField(verbose_name=u"Описание")
    isActive = models.BooleanField(verbose_name=u'Активно', default=True)

    class Meta:
        db_table = 'ourWorks'
        verbose_name = u'Наши работы'
        verbose_name_plural = u'Наши работы'

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return ('ourwork', (), {'ourwork_slug': self.slug})

class imageForOurWorkModel(models.Model):
    toWork = models.ForeignKey(ourWorksModel, verbose_name=u'Наши работы', related_name='image_ourwork')
    image = models.ImageField(upload_to='images/ourwork/', verbose_name=u"Изображение")

    class Meta:
        db_table = 'imagesOurworks'
        verbose_name = u'Изображение'
        verbose_name_plural = u'Изображения'