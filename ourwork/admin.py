# -*- coding: utf-8 -*-
from django.contrib import admin
from ourwork.models import ourWorksModel
from ourwork.models import imageForOurWorkModel

class imageForOurWorkStackedAdmin(admin.StackedInline):
    model = imageForOurWorkModel

class ourWorksAdmin(admin.ModelAdmin):
    list_display = ('name', 'isActive',)
    list_display_links = ('name',)
    list_per_page = 50
    ordering = ['name']
    search_fields = ['name']

    inlines = [imageForOurWorkStackedAdmin,]

admin.site.register(ourWorksModel, ourWorksAdmin)