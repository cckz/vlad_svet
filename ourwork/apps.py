# -*- coding: utf-8 -*-
from django.apps import AppConfig

class ourworkConfig(AppConfig):
    name = 'ourwork'
    verbose_name = u'Наши работы'
