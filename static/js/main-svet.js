$(document).foundation();

/*настройка  CSRF токен  для ajax формы*/
$.ajaxSetup({ 
     beforeSend: function(xhr, settings) {
         function getCookie(name) {
             var cookieValue = null;
             if (document.cookie && document.cookie != '') {
                 var cookies = document.cookie.split(';');
                 for (var i = 0; i < cookies.length; i++) {
                     var cookie = jQuery.trim(cookies[i]);
                     // Does this cookie string begin with the name we want?
                 if (cookie.substring(0, name.length + 1) == (name + '=')) {
                     cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                     break;
                 }
             }
         }
         return cookieValue;
         }
         if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
             // Only send the token to relative URLs i.e. locally.
             xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
         }
     } 
});

/*получение  CSRF токена*/
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

var csrftoken = getCookie('csrftoken'); /* CSRF токен */


$('.showchild').click(function() {
			var clickitem = $(this);
            console.log(clickitem.parent().next());
			if(clickitem.parent().hasClass('active')) {
				clickitem.parent().removeClass('active');
                clickitem.parent().next().addClass('visuallyhidden');
                clickitem.parent().next().one('transitionend', function(e) {
                    clickitem.parent().next().addClass('hide-childs');
                });
			} else {
				clickitem.parent().addClass('active');
                clickitem.parent().next().removeClass('hide-childs');
                setTimeout(function () {
                    clickitem.parent().next().removeClass('visuallyhidden');
                }, 20);
			}
		});

function send() {
    var n_ame = $('#id_name').val();
    var t_elephone =  $('#id_telephone').val();
    var info = $('#id_info').val();
    $('#id_name').val('');
    $('#id_telephone').val('');
    $('#id_info').val('');
    
    $.ajax({
    	csrfmiddlewaretoken : csrftoken,
        url : "/sender/",
        type : "POST",
        data : {telephone: t_elephone, name : n_ame, info: info},

        success : function(json) {
            console.log("success");
            $('#callModal').foundation('close');
        }
    });
}

$('#callModal').on('submit', function(event){
    event.preventDefault();
    send();
});

function login() {
    document.getElementById('span-hidden-pw-login').className= "hidden";
    document.getElementById('span-hidden-pw-login').innerHTML='';

    var email = $('#id_email').val();
    var password = $('#id_password').val();

    $.ajax({
        csrfmiddlewaretoken : csrftoken,
        url : "/account/login/",
        type : "POST",
        data : {email: email, password : password,},

        success : function() {
            location.reload()
        },

        error : function(xhr) {
            error_msg = JSON.parse(xhr.responseText);
            for (er in error_msg) {
                if (er == 'email') {
                    document.getElementById('span-hidden-em-login').className= "alert label";
                    document.getElementById('span-hidden-em-login').innerHTML=error_msg['email'];
                } else if (er == 'password') {
                    document.getElementById('span-hidden-pw-login').className = "alert label";
                    document.getElementById('span-hidden-pw-login').innerHTML = error_msg['password'];
                } else if (er == '__all__') {
                    document.getElementById('span-hidden-pw-login').className = "alert label";
                    document.getElementById('span-hidden-pw-login').innerHTML = error_msg['__all__'];
                }

            }
        }
    });
}

/*обработчик формы логина*/
$('#login-form').on('submit', function(event){
    event.preventDefault();
    login();
});

function registration() {
    document.getElementById('span-hidden-pw-reg').className= "hidden";
    document.getElementById('span-hidden-em-reg').className= "hidden";

    document.getElementById('span-hidden-pw-reg').innerHTML='';
    document.getElementById('span-hidden-em-reg').innerHTML='';

    var phone = $('#id_reg-phone').val();
    var passwordOne = $('#id_reg-password1').val();
    var passwordTwo = $('#id_reg-password2').val();
    var email = $('#id_reg-email').val();
    var last = $('#id_reg-last_name').val();
    var first = $('#id_reg-first_name').val();

    $.ajax({
        csrfmiddlewaretoken : csrftoken,
        url : "/account/register/",
        type : "POST",
        data : {username: email, password1 : passwordOne, password2: passwordTwo, email: email,
                first_name: first, last_name: last, phone: phone},

        success : function() {
            location.reload()
        },

        error : function(xhr) {
            error_msg = JSON.parse(xhr.responseText);
            for (er in error_msg) {
                if (er == 'password2') {
                    document.getElementById('span-hidden-pw-reg').className= "alert label";
                    document.getElementById('span-hidden-pw-reg').innerHTML=error_msg['password2'];
                } else if (er == 'email') {
                    document.getElementById('span-hidden-em-reg').className= "alert label";
                    document.getElementById('span-hidden-em-reg').innerHTML=error_msg['email'];
                }
            }
        }
    });
}

/*обработчик регистрационной формы*/
$('#registration-form').on('submit', function(event){
    event.preventDefault();
    registration();
});

/*Lightbox*/
$( function() {
    var activityIndicatorOn = function () {
        $('<div id="imagelightbox-loading"><div></div></div>').appendTo('body');
    },
    activityIndicatorOff = function() {
        $('#imagelightbox-loading').remove();
    };

    $('a[data-imagelightbox="a"]').imageLightbox({
        onLoadStart: function () {
            activityIndicatorOn();
        },
        onLoadEnd: function () {
            activityIndicatorOff();
        },
        onEnd: function () {
            activityIndicatorOff();
        }
    });

    $('a[data-imagelightbox="cert"]').imageLightbox({
        onLoadStart: function () {
            activityIndicatorOn();
        },
        onLoadEnd: function () {
            activityIndicatorOff();
        },
        onEnd: function () {
            activityIndicatorOff();
        }
    });
});
