# -*- coding: utf-8 -*-
from django.apps import AppConfig

class dictsConfig(AppConfig):
    name = 'dicts'
    verbose_name = u'Справочники'