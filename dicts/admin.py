# -*- coding: utf-8 -*-
from django.contrib import admin

from dicts.models import wt
from dicts.models import tok
from dicts.models import cokol
from dicts.models import volts
from dicts.models import outWt
from dicts.models import wtOneM
from dicts.models import sizes
from dicts.models import colors
from dicts.models import outVolts
from dicts.models import countShM
from dicts.models import complects
from dicts.models import svetPotok

from dicts.models import colorTempr
from dicts.models import upravlenie
from dicts.models import countryFirm
from dicts.models import securityStep
from dicts.models import forсeAmperOut
from dicts.models import productionFirm
from dicts.models import colorSvechenia

from dicts.models import ves
from dicts.models import material
from dicts.models import koefPuls
from dicts.models import tempDiapazon
from dicts.models import classDefence
from dicts.models import indexColor
from dicts.models import srokSlujbi
from dicts.models import tipKrivoi


#class dictsAdmin(admin.ModelAdmin):
#    def get_model_perms(self, request):
#        return {}


admin.site.register(wt)
admin.site.register(cokol)
admin.site.register(volts)
admin.site.register(tok)
admin.site.register(outWt)
admin.site.register(sizes)
admin.site.register(wtOneM)
admin.site.register(colorTempr)
admin.site.register(svetPotok)
admin.site.register(productionFirm)
admin.site.register(countryFirm)
admin.site.register(colorSvechenia)
admin.site.register(colors)
admin.site.register(outVolts)
admin.site.register(countShM)
admin.site.register(complects)
admin.site.register(upravlenie)
admin.site.register(securityStep)
admin.site.register(forсeAmperOut)

admin.site.register(classDefence)
admin.site.register(indexColor)
admin.site.register(srokSlujbi)
admin.site.register(tipKrivoi)
admin.site.register(tempDiapazon)
admin.site.register(koefPuls)
admin.site.register(material)
admin.site.register(ves)