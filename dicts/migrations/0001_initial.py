# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-01-16 10:32
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='cokol',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True, verbose_name='Значение')),
            ],
            options={
                'verbose_name': 'Цоколь',
                'verbose_name_plural': 'Цоколь',
            },
        ),
        migrations.CreateModel(
            name='colorColb',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True, verbose_name='Значение')),
            ],
            options={
                'verbose_name': 'Цвет колбы',
                'verbose_name_plural': 'Цвет колбы',
            },
        ),
        migrations.CreateModel(
            name='colorSvechenia',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True, verbose_name='Значение')),
            ],
            options={
                'verbose_name': 'Цвет свечения',
                'verbose_name_plural': 'Цвет свечения',
            },
        ),
        migrations.CreateModel(
            name='colorTempr',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True, verbose_name='Значение')),
            ],
            options={
                'verbose_name': 'Цветовая температура',
                'verbose_name_plural': 'Цветовая температурат',
            },
        ),
        migrations.CreateModel(
            name='countryFirm',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True, verbose_name='Значение')),
            ],
            options={
                'verbose_name': 'Страна производства',
                'verbose_name_plural': 'Страна производства',
            },
        ),
        migrations.CreateModel(
            name='dlinna',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True, verbose_name='Значение')),
            ],
            options={
                'verbose_name': 'Длина изделия, мм',
                'verbose_name_plural': 'Длина изделия, мм',
            },
        ),
        migrations.CreateModel(
            name='productionFirm',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True, verbose_name='Значение')),
            ],
            options={
                'verbose_name': 'Производитель',
                'verbose_name_plural': 'Производитель',
            },
        ),
        migrations.CreateModel(
            name='shirina',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True, verbose_name='Значение')),
            ],
            options={
                'verbose_name': 'Ширина изделия, мм',
                'verbose_name_plural': 'Ширина изделия, мм',
                'db_table': 'dlins',
            },
        ),
        migrations.CreateModel(
            name='svetPotok',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True, verbose_name='Значение')),
            ],
            options={
                'verbose_name': 'Световой поток, Lm',
                'verbose_name_plural': 'Световой поток, Lm',
            },
        ),
        migrations.CreateModel(
            name='typeLamp',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True, verbose_name='Значение')),
            ],
            options={
                'verbose_name': 'Тип лампы',
                'verbose_name_plural': 'Тип лампы',
            },
        ),
        migrations.CreateModel(
            name='vidLamp',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True, verbose_name='Значение')),
            ],
            options={
                'verbose_name': 'Вид лампы',
                'verbose_name_plural': 'Вид лампы',
            },
        ),
        migrations.CreateModel(
            name='volts',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True, verbose_name='Значение')),
            ],
            options={
                'verbose_name': 'Напряжение, Вольт',
                'verbose_name_plural': 'Напряжение, Вольт',
            },
        ),
        migrations.CreateModel(
            name='wt',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True, verbose_name='Значение')),
            ],
            options={
                'verbose_name': 'Мощность, Вт',
                'verbose_name_plural': 'Мощность, Вт',
            },
        ),
    ]
