# -*- coding: utf-8 -*-
from django.db import models

class abstractDicts(models.Model):
    name = models.CharField(max_length=150, verbose_name=u"Значение",
                            unique=True)

    class Meta:
        abstract = True

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

#Количество, шт/м
class countShM(abstractDicts):
    class Meta:
        verbose_name = u'Количество, шт/м'
        verbose_name_plural = u"Количество, шт/м"

#Мощность
class wtOneM(abstractDicts):
    class Meta:
        verbose_name = u'Мощность, 1м/Вт (LED)'
        verbose_name_plural = u"Мощность, 1м/Вт (LED)"

#Напряжение
class volts(abstractDicts):
    class Meta:
        verbose_name = u'Напряжение, Вольт'
        verbose_name_plural = u"Напряжение, Вольт"

#Степень защиты
class securityStep(abstractDicts):
    class Meta:
        verbose_name = u'Степень защиты'
        verbose_name_plural = u"Степень защиты"

#Цвет
class colors(abstractDicts):
    class Meta:
        verbose_name = u'Цвет'
        verbose_name_plural = u"Цвет"

#Размеры, мм
class sizes(abstractDicts):
    class Meta:
        verbose_name = u'Размеры, мм'
        verbose_name_plural = u"Размеры, мм"

#Ток на выходе, А
class forсeAmperOut(abstractDicts):
    class Meta:
        verbose_name = u'Ток на выходе, А'
        verbose_name_plural = u"Ток на выходе, А"

#Напряжение на выходе
class outVolts(abstractDicts):
    class Meta:
        verbose_name = u'Выходное напряжение, В'
        verbose_name_plural = u"Выходное напряжение, В"

# Мощность
class wt(abstractDicts):
    class Meta:
        verbose_name = u'Мощность, Вт'
        verbose_name_plural = u"Мощность, Вт"

#Управление
class upravlenie(abstractDicts):
    class Meta:
        verbose_name = u'Управление'
        verbose_name_plural = u"Управление"

#Ток, А
class tok(abstractDicts):
    class Meta:
        verbose_name = u'Ток, А'
        verbose_name_plural = u"Ток, А"

#Выходная мощность
class outWt(abstractDicts):
    class Meta:
        verbose_name = u'Выходная мощность, Вт'
        verbose_name_plural = u"Выходная мощность, Вт"

# Цвет свечения
class colorSvechenia(abstractDicts):
    class Meta:
        verbose_name = u'Цвет свечения'
        verbose_name_plural = u'Цвет свечения'

# Комплект
class complects(abstractDicts):
    class Meta:
        verbose_name = u'Комплект'
        verbose_name_plural = u"Комплект"

# Цветовая температура
class colorTempr(abstractDicts):
    class Meta:
        verbose_name = u'Цветовая температура'
        verbose_name_plural = u"Цветовая температура"

# Световой поток, Lm
class svetPotok(abstractDicts):
    class Meta:
        verbose_name = u'Световой поток, Lm'
        verbose_name_plural = u'Световой поток, Lm'

#Производитель
class productionFirm(abstractDicts):
    class Meta:
        verbose_name = u'Производитель'
        verbose_name_plural = u"Производитель"

#Страна производства
class countryFirm(abstractDicts):
    class Meta:
        verbose_name = u'Страна производства'
        verbose_name_plural = u"Страна производства"

#Цоколь
class cokol(abstractDicts):
    class Meta:
        verbose_name = u'Цоколь'
        verbose_name_plural = u"Цоколь"
        
#Коэфициент пульсации
class koefPuls(abstractDicts):
    class Meta:
        verbose_name = u'Коэфициент пульсации'
        verbose_name_plural = u"Коэфициент пульсации"
        
#материал
class material(abstractDicts):
    class Meta:
        verbose_name = u'Материал'
        verbose_name_plural = u"Материал"
        
#температурный диапазон
class tempDiapazon(abstractDicts):
    class Meta:
        verbose_name = u'Температурный диапазон'
        verbose_name_plural = u"Температурный диапазон"
        
#класс защиты
class classDefence(abstractDicts):
    class Meta:
        verbose_name = u'Класс защиты'
        verbose_name_plural = u"Класс защиты"
        
#индекс цветопередачи
class indexColor(abstractDicts):
    class Meta:
        verbose_name = u'Индекс цветопередачи'
        verbose_name_plural = u"Индекс цветопередачи"
        
#срок службы, ч
class srokSlujbi(abstractDicts):
    class Meta:
        verbose_name = u'срок службы, ч'
        verbose_name_plural = u"срок службы, ч"
        
#Вес
class ves(abstractDicts):
    class Meta:
        verbose_name = u'Вес'
        verbose_name_plural = u"Вес"
        
#тип кривой ксс
class tipKrivoi(abstractDicts):
    class Meta:
        verbose_name = u'тип кривой ксс'
        verbose_name_plural = u"тип кривой ксс"