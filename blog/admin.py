# -*- coding: utf-8 -*-
from django.contrib import admin
from blog.models import blogModel

class blogAdmin(admin.ModelAdmin):
    list_display = ('name', 'isActive', 'created_at', )
    list_display_links = ('name',)
    list_per_page = 50
    ordering = ['-created_at']
    search_fields = ['name', 'created_at']

admin.site.register(blogModel, blogAdmin)