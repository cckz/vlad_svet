# -*- coding: utf-8 -*-
from django.apps import AppConfig

class blogConfig(AppConfig):
    name = 'blog'
    verbose_name = u'Наш блог'
