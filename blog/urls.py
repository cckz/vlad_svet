# -*- coding: utf-8 -*-
import blog.views
from django.conf.urls import url

urlpatterns = [
    url(r'^$', blog.views.showBlog, {'template_name': 'blog/blog.html'},
        name='blog'),

    url(r'^(?P<row_slug>[-\w]+)/$', blog.views.showRow,
        {'template_name': 'blog/row.html'}, name='row_blog'),
]

