# -*- coding: utf-8 -*-
from django.db import models
from autoslug import AutoSlugField
from tinymce.models import HTMLField

class blogModel(models.Model):
    name = models.CharField(max_length=250, unique=True, verbose_name=u"Заголовок")
    slug = AutoSlugField(populate_from='name', unique=True)
    title_row = HTMLField(verbose_name=u'Сокращенный контент')
    content = HTMLField(verbose_name=u'Контент')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Дата создания")
    isActive = models.BooleanField(verbose_name=u'Активно', default=True)

    class Meta:
        db_table = 'blog'
        ordering = ['-created_at']
        verbose_name = u'Наш блог'
        verbose_name_plural = u'Наш блог'

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return ('row_blog', (), {'row_slug': self.slug})