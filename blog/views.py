# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.core.paginator import EmptyPage
from django.core.paginator import Paginator
from django.core.paginator import PageNotAnInteger

from blog.models import blogModel

def showBlog(request, template_name):
    blog = blogModel.objects.filter(isActive=True)
    page_title = u'Наш блог'

    paginator = Paginator(blog, 12)
    page = request.GET.get('page')
    try:
        rows = paginator.page(page)
    except PageNotAnInteger:
        rows = paginator.page(1)
    except EmptyPage:
        rows = paginator.page(paginator.num_pages)
    return render(request, template_name, locals())

def showRow(request, row_slug, template_name):
    row_blog = get_object_or_404(blogModel, slug=row_slug)
    page_title = row_blog.name
    return render(request, template_name, locals())