# -*- coding: utf-8 -*-
from django.apps import AppConfig

class extFlatpageConfig(AppConfig):
    name = 'extFlatpage'
    verbose_name = u'Статичные страницы'
