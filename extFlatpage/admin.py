# -*- coding: utf-8 -*-
from django.contrib import admin
from extFlatpage.forms import extendFlatPageForm
from django.contrib.flatpages.models import FlatPage
from django.contrib.flatpages.admin import FlatPageAdmin

class extendFlatPageAdmin(FlatPageAdmin):
    form = extendFlatPageForm
    list_display = ['title', 'url']

    fieldsets = (
        (
            None, {'fields': ('url', 'title', 'content', 'sites', 'template_name'),}
        ),
    )

admin.site.unregister(FlatPage)
admin.site.register(FlatPage, extendFlatPageAdmin)