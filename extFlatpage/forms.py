# -*- coding: utf-8 -*-
from django import forms
from tinymce.widgets import TinyMCE
from django.contrib.flatpages.models import FlatPage
from django.contrib.flatpages.admin import FlatpageForm

class extendFlatPageForm(FlatpageForm):
    class Meta:
        model = FlatPage
        fields = '__all__'

    content = forms.CharField(widget=TinyMCE(attrs={'cols': 90, 'rows': 30}))