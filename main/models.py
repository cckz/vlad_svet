# -*- coding: utf-8 -*-
from django.db import models
from tinymce.models import HTMLField

class contactsModel(models.Model):
    defaultDict = {'name': u'ИП Зубов Владислав Сергеевич',
                   'street': u'160029, г.Вологда, ул.Прокатова, д.5, кв.111',
                   'inn': u'352601377742',
                   'bic': u'041909644',
                   'bicDecrpt': 'Отделение № 8638 Сбербанка России г.Вологда',
                   'rs': '40802810312000000863',
                   'ks': '30101810900000000644',
                   'ogrn': '311353817900029',
                   'phone': '+7(953)519-40-34',
                   'email': 'info@lumen35.ru'}

    name = models.CharField(verbose_name=u'Наименование', max_length=250, default=defaultDict['name'])
    street = models.CharField(verbose_name=u'Адресс', max_length=255, default=defaultDict['street'])
    phone = models.CharField(verbose_name=u'Телефон', blank=True, max_length=16, default=defaultDict['phone'])
    email = models.EmailField(verbose_name=u'Электронная почта', default=defaultDict['email'])
    isActive = models.BooleanField(verbose_name=u'Активно', default=True,)

    class Meta:
        db_table = 'contacts'
        ordering = ['name']
        verbose_name = u'Контакты'
        verbose_name_plural = u'Контакты'

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

class price(models.Model):
    name = models.CharField(verbose_name=u'Наименование', max_length=50, default=u'Оптовый прайс')
    price = models.FileField(verbose_name=u'Прайс', upload_to='prices')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=u'Дата создания')
    update_at = models.DateTimeField(auto_now=True, verbose_name=u'Дата обновления')

    class Meta:
        db_table = 'prices'
        ordering = ['name']
        verbose_name = u'Прайс'
        verbose_name_plural = u'Прайс'

    def __unicode__(self):
        return self.price.name

    def __str__(self):
        return self.price.name

class slider(models.Model):
    name = models.CharField(verbose_name=u'Наименование', max_length=255)
    image = models.ImageField(verbose_name=u'Изображение для слайдера', upload_to='images/slider/')
    url = models.URLField(verbose_name=u'Ссылка')
    isActive = models.BooleanField(verbose_name=u'Активно', default=True)

    class Meta:
        db_table = 'slider'
        ordering = ['name']
        verbose_name = u'Слайдер'
        verbose_name_plural = u'Слайдер'

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

class mainPageModel(models.Model):
    name = models.CharField(verbose_name=u'Заголовок', max_length=250)
    content = HTMLField(verbose_name=u'Контент')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Дата создания")
    isActive = models.BooleanField(verbose_name=u'Активно', default=True)

    class Meta:
        db_table = 'mainpage'
        ordering = ['-created_at']
        verbose_name = u'Контент главной'
        verbose_name_plural = u'Контент главной'

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

class certificate(models.Model):
    name = models.CharField(verbose_name=u'Наименование', max_length=150)

    class Meta:
        db_table = 'certificates'
        verbose_name = u'Сертификат'
        verbose_name_plural = u'Сертификаты'

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

class imageCertificate(models.Model):
    toCerteficate = models.ForeignKey(certificate, verbose_name=u'Наименование', related_name='image_certificate')
    image = models.ImageField(upload_to='images/certificate/', verbose_name=u"Изображение")

    class Meta:
        db_table = 'images certificates'
        verbose_name = u'Изображение'
        verbose_name_plural = u'Изображения'

    def __unicode__(self):
        return str(self.id)

    def __str__(self):
        return str(self.id)


class globalSEO(models.Model):
    site_name = models.CharField(verbose_name=u'Заголовок сайта', max_length=250)
    meta_keyword = models.TextField(verbose_name=u'Keyword', blank=True)
    meta_description = models.TextField(verbose_name=u'Description', blank=True)

    class Meta:
        db_table = 'SEOS'
        verbose_name = u'Глобальные настройки SEO'
        verbose_name_plural = u'Глобальные настройки SEO'

    def __unicode__(self):
        return self.site_name

    def __str__(self):
        return self.site_name