#-*- coding: utf-8 -*-
from django import forms
from main.models import contactsModel

#не используется
class contactsForm(forms.ModelForm):
    class Meta:
        model = contactsModel
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(contactsForm, self).__init__(*args, **kwargs)
        self.fields['inn'].widget.attrs['style'] = 'width:20em;'
        self.fields['bic'].widget.attrs['style'] = 'width:20em;'
        self.fields['ogrn'].widget.attrs['style'] = 'width:20em;'
        self.fields['rs'].widget.attrs['style'] = 'width:20em;'
        self.fields['ks'].widget.attrs['style'] = 'width:20em;'