# -*- coding: utf-8 -*-
from django.apps import AppConfig

class coreConfig(AppConfig):
    name = 'main'
    verbose_name = u'Главная'
