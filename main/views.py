# -*- coding: utf-8 -*-
import os
from unidecode import unidecode
from svet_vlad.settings import MEDIA_ROOT
from main.models import price
from main.models import slider
from main.models import mainPageModel
from main.models import certificate

from django.core import urlresolvers
from django.shortcuts import render
from django.shortcuts import HttpResponse
from django.http import HttpResponseRedirect
from django.core.paginator import EmptyPage
from django.core.paginator import Paginator
from django.core.paginator import PageNotAnInteger

def mainPage(request, template_name):
    page_name = u'Главная'
    allContent = mainPageModel.objects.filter(isActive=True)

    paginator = Paginator(allContent, 16)
    page = request.GET.get('page')
    try:
        contents = paginator.page(page)
    except PageNotAnInteger:
        contents = paginator.page(1)
    except EmptyPage:
        contents = paginator.page(paginator.num_pages)

    slides = slider.objects.filter(isActive=True)
    return render(request, template_name, locals())

def download_price(request):
    if request.user.is_authenticated():
        downloadPrice = price.objects.get(name__icontains=u'Оптовый прайс').price
        file_path = u'%s/%s' % (MEDIA_ROOT, downloadPrice.name)
        filename = unidecode(os.path.basename(file_path))
        with open(file_path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
            response['Content-Disposition'] = u'attachment; filename=%s' % (filename)
            return response
    else:
        redirectUrl = urlresolvers.reverse('main')
        return HttpResponseRedirect(redirectUrl)

def certificatesPage(request, template_name):
    cert = certificate.objects.all()[0]
    page_name = cert.name
    return render(request, template_name, locals())