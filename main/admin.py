# -*- coding: utf-8 -*-
from django.contrib import admin
from main.models import contactsModel
from main.forms import contactsForm
from main.models import price, slider
from main.models import mainPageModel
from main.models import certificate
from main.models import imageCertificate

class contactsAdmin(admin.ModelAdmin):
    list_display = ('name', 'phone', 'email', 'isActive')
    list_display_links = ('name',)
    list_per_page = 50
    ordering = ['name']
    search_fields = ['name']

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

admin.site.register(contactsModel, contactsAdmin)

class priceAdmin(admin.ModelAdmin):
    list_display = ('name', 'update_at', 'created_at',)
    list_display_links = ('name',)
    list_per_page = 50
    ordering = ['name']

admin.site.register(price, priceAdmin)

class sliderAdmin(admin.ModelAdmin):
    list_display = ('name', 'isActive',)
    list_display_links = ('name',)
    list_per_page = 50
    ordering = ['name']

admin.site.register(slider, sliderAdmin)

class mainPageAdmin(admin.ModelAdmin):
    list_display = ('name', 'isActive', 'created_at')
    list_display_links = ('name',)
    list_per_page = 50
    ordering = ['name']

admin.site.register(mainPageModel, mainPageAdmin)

class imageCertificateAdmin(admin.StackedInline):
    model = imageCertificate

class certificateAdmin(admin.ModelAdmin):
    list_display = ('name',)
    inlines = [imageCertificateAdmin,]

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

admin.site.register(certificate, certificateAdmin)
