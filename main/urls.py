# -*- coding: utf-8 -*-
from django.conf.urls import url
from main.views import mainPage
from main.views import download_price
from main.views import certificatesPage
from django.views.generic import TemplateView
from django.contrib.sitemaps.views import sitemap

from utilits.sitemap import productSitemap
from utilits.sitemap import categorySitemap
from utilits.sitemap import categoryClassSitemap
from utilits.sitemap import flatSitemap
from utilits.sitemap import ourWorksSitemap
from utilits.sitemap import certificateSitemap
from utilits.sitemap import blogSitemap

sitemaps = {'category': categorySitemap,
            'categoryClass': categoryClassSitemap,
            'product': productSitemap,
            'flatpages': flatSitemap,
            'ourworks': ourWorksSitemap,
            'certificate': certificateSitemap,
            'blog': blogSitemap,}

urlpatterns = [
    url(r'^download/$', download_price,
        name='download_price'),
    url(r'^$', mainPage, {'template_name': 'main.html'},
        name='main'),
    url(r'^certificate/$', certificatesPage, {'template_name': 'certificates/certificate.html'},
        name='certificate'),
    url(r'^robots.txt$', TemplateView.as_view(template_name="robots.txt", content_type="text/plain"),
        name="robots_file"),
    url(r'^sitemap.xml/$', sitemap, {'sitemaps': sitemaps},
        name='django.contrib.sitemaps.views.sitemap'),
]