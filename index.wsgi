# -*- coding: utf-8 -*-

import os, sys, site

import time
import traceback
import signal

activate_this = '/home/l/lumen35ru/.venv/bin/activate_this.py'
#execfile(activate_this, dict(__file__=activate_this))
exec(compile(open(activate_this, "rb").read(), activate_this, 'exec'), dict(__file__=activate_this))
site.addsitedir('/home/l/lumen35ru/.venv/lib/python3.4/site-packages')
sys.path.insert(1,'/home/l/lumen35ru/lumen35.ru/')

import django

if django.VERSION[1] <= 6:
    os.environ['DJANGO_SETTINGS_MODULE'] = 'svet_vlad.settings'
    import django.core.handlers.wsgi
    application = django.core.handlers.wsgi.WSGIHandler()
else:
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "svet_vlad.settings")
    from django.core.wsgi import get_wsgi_application
    try:
        application = get_wsgi_application()
    except RuntimeError:
        traceback.print_exc()
        os.kill(os.getpid(), signal.SIGINT)
        time.sleep(2.5)

