import React from 'react';
import { render } from 'react-dom'

import { TopPanel, MiddlePanel, FooterPanel } from './layout'

class App extends React.Component {
  render () {
    return (
        <div>
          <TopPanel />
          <MiddlePanel />
          <FooterPanel />
        </div>

    )
  }
}

render(<App />, document.getElementById('app'))
