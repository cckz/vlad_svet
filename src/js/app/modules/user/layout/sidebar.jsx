import React from 'react'

import { Navigation } from './'

export class SidebarPanel extends React.Component {
    render() {
        return (
            <Navigation />
        )
    }
}
