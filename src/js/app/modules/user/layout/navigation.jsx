import React from 'react'

import { Menu, MenuItem } from 'react-foundation'

export class Navigation extends React.Component {
    render() {
        return (
            <Menu isVertical>
                <MenuItem>
                    <a>Заявки</a>
                </MenuItem>
                <MenuItem>
                    <a>Финансы</a>
                </MenuItem>
                <MenuItem>
                    <a>Настройки</a>
                </MenuItem>
                <MenuItem>
                    <a>Сообщения</a>
                </MenuItem>
            </Menu>
        )
    }
}
