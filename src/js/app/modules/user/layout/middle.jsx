import React from 'react'
import { Row, Column } from 'react-foundation'

import {SidebarPanel, ContentPanel} from './'

export class MiddlePanel extends React.Component {
    render() {
        return (
            <Row className="display">
                <Column small={2}>
                    <SidebarPanel/>
                </Column>
                <Column small={10}>
                    <ContentPanel/>
                </Column>
            </Row>
        )
    }
}
