# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.core.paginator import EmptyPage
from django.core.paginator import Paginator
from django.core.paginator import PageNotAnInteger

from catalog.models import productModel
from catalog.models import categoryModel
from catalog.models import categoryClassModel

def catalogShow(request, template_name):
    page_title = u'Каталог товаров'
    #local_metaDisc = page_title
    return render(request, template_name, locals())

def categoryShow(request, category_slug, template_name):
    getCategory = get_object_or_404(categoryModel, slug=category_slug)
    getCategoryClass = getCategory.categoryclassmodel_set.all()

    page_title = getCategory.name

    #form = filterCategoryForm()
    #breadcrumbs = categoryGet
    #breadcrumbsName = breadcrumbs.name
    return render(request, template_name, locals())

def categoryClassShow(request, category_class_slug, template_name):
    getCategoryClass = get_object_or_404(categoryClassModel, slug=category_class_slug)
    page_title = getCategoryClass.name
    allProducts = productModel.objects.filter(toCategoryCalss=getCategoryClass, isActive=True)

    paginator = Paginator(allProducts, 16)
    page = request.GET.get('page')
    try:
        products = paginator.page(page)
    except PageNotAnInteger:
        products = paginator.page(1)
    except EmptyPage:
        products = paginator.page(paginator.num_pages)

    #form = filterCategoryForm()
    #breadcrumbs = categoryGet
    #breadcrumbsName = breadcrumbs.name
    return render(request, template_name, locals())

def productShow(request, product_slug, template_name):
    getProduct = get_object_or_404(productModel, slug=product_slug)
    page_title = getProduct.name
    return render(request, template_name, locals())