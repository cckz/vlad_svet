# -*- coding: utf-8 -*-
from django.contrib import admin

from catalog.models import productModel
from catalog.models import categoryModel
from catalog.models import imageForProduct
from catalog.models import categoryClassModel

from catalog.forms import productAdminForm

class categoryClassStackedAdmin(admin.StackedInline):
    model = categoryClassModel

class imageForProductStackedAdmin(admin.StackedInline):
    model = imageForProduct

class categoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'isActive')
    list_display_links = ('name',)
    list_per_page = 50
    ordering = ['name']
    search_fields = ['name']

    inlines = [categoryClassStackedAdmin,]

admin.site.register(categoryModel, categoryAdmin)

class productAdmin(admin.ModelAdmin):
    form = productAdminForm
    fields = [u'get_fields']

    list_display = ('name', 'price', 'oldPrice', 'created_at', 'isActive',)
    list_display_links = ('name',)
    list_per_page = 50
    ordering = ['-created_at']
    search_fields = ['name', 'articul',]

    exclude = ('created_at',)
    readonly_fields = ('params',)

    inlines = [imageForProductStackedAdmin,]

    def get_fields(self, request, obj):
        self.fields = ['name', 'toCategoryCalss', 'isActive', 'articul', 'garantia', 'price', 'oldPrice', 'toProduction',
                       'toCountry',]
        if obj:
            if not obj.params:
                self.fields.append('paramsChoice')
            else:
                for param in obj.params.split(';'):
                    self.fields.append(param)
                #self.fields.append('params')
                self.fields.append('resetParam')
        else:
            self.fields.append('paramsChoice')
        return self.fields

    def save_model(self, request, obj, form, change):
        if obj.params and form.cleaned_data['resetParam']:
           obj.params = ''
           obj.resetParam = False
        if form.cleaned_data['paramsChoice']:
            obj.params = ';'.join(form.cleaned_data['paramsChoice'])
        super(productAdmin, self).save_model(request, obj, form, change)

admin.site.register(productModel, productAdmin)
