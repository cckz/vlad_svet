# -*- coding: utf-8 -*-
from django.apps import AppConfig

class catalogConfig(AppConfig):
    name = 'catalog'
    verbose_name = u'Каталог'
