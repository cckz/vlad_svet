# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-06-20 19:33
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('dicts', '0004_classdefence_indexcolor_koefpuls_material_srokslujbi_tempdiapazon_tipkrivoi_ves'),
        ('catalog', '0010_auto_20170329_0902'),
    ]

    operations = [
        migrations.AddField(
            model_name='productmodel',
            name='toIndexColor',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='dicts.indexColor', verbose_name='Индекс цветопередачи'),
        ),
        migrations.AddField(
            model_name='productmodel',
            name='toKoefPuls',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='dicts.koefPuls', verbose_name='Коэфициент пульсации'),
        ),
        migrations.AddField(
            model_name='productmodel',
            name='toMaterial',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='dicts.material', verbose_name='Материал'),
        ),
        migrations.AddField(
            model_name='productmodel',
            name='toSrokSlujbi',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='dicts.srokSlujbi', verbose_name='Срок службы'),
        ),
        migrations.AddField(
            model_name='productmodel',
            name='toTempDiapazon',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='dicts.tempDiapazon', verbose_name='Температурный диапазон'),
        ),
        migrations.AddField(
            model_name='productmodel',
            name='toTipKrivoi',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='dicts.tipKrivoi', verbose_name='Тип кривой ксс'),
        ),
        migrations.AddField(
            model_name='productmodel',
            name='toVes',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='dicts.ves', verbose_name='Вес'),
        ),
        migrations.AddField(
            model_name='productmodel',
            name='toСlassDefence',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='dicts.classDefence', verbose_name='Класс защиты'),
        ),
    ]
