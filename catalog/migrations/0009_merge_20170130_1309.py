# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-01-30 10:09
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0008_auto_20170130_1220'),
        ('catalog', '0008_merge_20170127_1752'),
    ]

    operations = [
    ]
