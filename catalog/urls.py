# -*- coding: utf-8 -*-
import catalog.views
from django.conf.urls import url

urlpatterns = [
    url(r'^$', catalog.views.catalogShow, {'template_name': 'catalog/catalog.html'},
        name='catalog'),

    url(r'^(?P<category_slug>[-\w]+)/$', catalog.views.categoryShow,
        {'template_name': 'catalog/category.html'}, name='catalogCategory'),

    url(r'^category/(?P<category_class_slug>[-\w]+)/$', catalog.views.categoryClassShow,
        {'template_name': 'catalog/categoryClass.html'}, name='catalogCategoryClass'),

    url(r'^product/(?P<product_slug>[-\w]+)/$', catalog.views.productShow,
        {'template_name': 'catalog/product.html'}, name='catalogProduct'),
]

