# -*- coding: utf-8 -*-
from django import forms
from catalog.models import productModel
from catalog.models import categoryClassModel

class productAdminForm(forms.ModelForm):
    paramsList = [(u'toCountShM', u'Количество, шт/м (LED Ленты)'),
                  (u'toWtOneM', 'Мощность 1м, Вт (LED Ленты)'),
                  (u'toVolts', u'Напряжение, В'),
                  (u'toSecurityStep', u'Степень защиты'),
                  (u'toColors', u'Цвет'),
                  (u'toSizes', u'Размер, мм'),
                  (u'toForсeAmperOut', u'Ток на выходе, А (Источники питания)'),
                  (u'toOutVolts', u'Выходное напряжение, В'),
                  (u'toWt', u'Мощность'),
                  (u'toUpravlenie', u'Управление'),
                  (u'toTok', u'Ток, A'),
                  (u'toOutWt', u'Выходная мощность, W (Управление светом)'),
                  (u'toColorSvechenia', u'Цвет свечения'),
                  (u'toComplects', u'Комплект'),
                  (u'toColorTempr', u'Цветовая температура'),
                  (u'toSvetPotok', u'Световой поток, lm'),
                  (u'toCokol', u'Цоколь'),
                  
                  (u'toVes', u'Вес'),
                  (u'toIndexColor', u'Индекс цветопередачи'),
                  (u'toMaterial', u'Материал'),
                  (u'toClassDefence', u'Класс защиты'),
                  (u'toTipKrivoi', u'Тип кривой ксс'),
                  (u'toKoefPuls', u'Коэфициент пульсации'),
                  (u'toTempDiapazon', u'Температурный диапазон'),
                  (u'toSrokSlujbi', u'Срок службы'),
                  
                  ]
    paramsList.sort(key=lambda tup: tup[1])
    paramsChoice = forms.MultipleChoiceField(choices=paramsList, label=u'Выберите параметры', required=False)

    class Meta:
        model = productModel
        fields = "__all__"

    def clean_price(self):
        if self.cleaned_data['price'] <= 0:
            raise forms.ValidationError(u'Цена должна быть больше 0')
        return self.cleaned_data['price']


        #class BookForm(forms.ModelForm):
        #    class Meta:

                #model = Book

#        def __init__(self, *args, **kwargs):
 #           super(BookForm, self).__init__(*args, **kwargs)

  #      for f in some_fields:
   #         self.fields[f] = forms.CharField(max_length=10)