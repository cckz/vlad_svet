# -*- coding: utf-8 -*-
from django.db import models
from autoslug import AutoSlugField

from dicts.models import wt
from dicts.models import tok
from dicts.models import cokol
from dicts.models import volts
from dicts.models import outWt
from dicts.models import wtOneM
from dicts.models import sizes
from dicts.models import colors
from dicts.models import outVolts
from dicts.models import countShM
from dicts.models import complects
from dicts.models import svetPotok

from dicts.models import colorTempr
from dicts.models import upravlenie
from dicts.models import countryFirm
from dicts.models import securityStep
from dicts.models import forсeAmperOut
from dicts.models import productionFirm
from dicts.models import colorSvechenia

from dicts.models import ves
from dicts.models import material
from dicts.models import koefPuls
from dicts.models import tempDiapazon
from dicts.models import classDefence
from dicts.models import indexColor
from dicts.models import srokSlujbi
from dicts.models import tipKrivoi

class categoryModel(models.Model):
    name = models.CharField(max_length=80, unique=True, verbose_name=u"Наименование")
    slug = AutoSlugField(populate_from='name', unique=True)
    image = models.ImageField(upload_to='images/category/', verbose_name=u"Изображение", blank=True)
    isActive = models.BooleanField(default=True, verbose_name=u'Активно')

    class Meta:
        db_table = 'categories'
        verbose_name = u'Категория'
        verbose_name_plural = u'Категории'

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return ('catalogCategory', (), {'category_slug': self.slug})

class categoryClassModel(models.Model):
    name = models.CharField(max_length=80, verbose_name=u"Наименование")
    slug = AutoSlugField(populate_from='name', unique=True)
    categoriesClass = models.ForeignKey(categoryModel, verbose_name=u"Категория", null=True)
    image = models.ImageField(upload_to='images/category/', verbose_name=u"Изображение")
    isActive = models.BooleanField(default=True, verbose_name=u'Активно')

    class Meta:
        db_table = 'categories_class'
        verbose_name = u'Подклассы товара'
        verbose_name_plural = u'Подклассы товаров'

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return ('catalogCategoryClass', (), {'category_class_slug': self.slug})

class productModel(models.Model):
    name = models.CharField(max_length=255, verbose_name=u"Наименование")
    slug = AutoSlugField(populate_from='name', unique=True)
    toCategoryCalss = models.ForeignKey(categoryClassModel, verbose_name=u"Категория")

    isActive = models.BooleanField(verbose_name=u"Активно", default=True)

    articul = models.CharField(max_length=70, verbose_name=u"Артикул")
    price = models.DecimalField(max_digits=9, decimal_places=0, verbose_name=u'Цена')
    oldPrice = models.DecimalField(max_digits=9, decimal_places=0, blank=True, default=0, verbose_name=u'Старая цена')

    created_at = models.DateTimeField(auto_now_add=True, verbose_name=u'Дата создания')

    toProduction = models.ForeignKey(productionFirm, blank=True, null=True, verbose_name=u'Производитель',)
    toCountry = models.ForeignKey(countryFirm, blank=True, null=True, verbose_name=u'Страна производства')

    toCokol = models.ForeignKey(cokol, blank=True, null=True, verbose_name=u'Цоколь')
    toWt = models.ForeignKey(wt, blank=True, null=True, verbose_name=u'Мощность, Вт')
    toTok = models.ForeignKey(tok, blank=True, null=True, verbose_name=u'Ток, А')
    toVolts = models.ForeignKey(volts, blank=True, null=True, verbose_name=u'Напряжение, Вольт')
    toColorTempr = models.ForeignKey(colorTempr, blank=True, null=True, verbose_name=u'Цветовая температура')
    toSvetPotok = models.ForeignKey(svetPotok, blank=True, null=True, verbose_name=u'Световой поток, Lm')
    toOutWt = models.ForeignKey(outWt, blank=True, null=True, verbose_name=u'Выходная мощность')
    toColorSvechenia = models.ForeignKey(colorSvechenia, blank=True, null=True, verbose_name=u'Цвет свечения')
    toWtOneM = models.ForeignKey(wtOneM, blank=True, null=True, verbose_name=u'Мощность, 1м/Вт (LED)')
    toSizes = models.ForeignKey(sizes, blank=True, null=True, verbose_name=u'Размеры, мм')
    toColors = models.ForeignKey(colors, blank=True, null=True, verbose_name=u'Цвет')
    toOutVolts = models.ForeignKey(outVolts, blank=True, null=True, verbose_name=u'Выходное напряжение')
    toCountShM = models.ForeignKey(countShM, blank=True, null=True, verbose_name=u'Количество, шт/м (LED)')
    toComplects = models.ForeignKey(complects, blank=True, null=True, verbose_name=u'Комплект')
    toUpravlenie = models.ForeignKey(upravlenie, blank=True, null=True, verbose_name=u'Управление')
    toSecurityStep = models.ForeignKey(securityStep, blank=True, null=True, verbose_name=u'Степень защиты')
    toForсeAmperOut = models.ForeignKey(forсeAmperOut, blank=True, null=True, verbose_name=u'Ток на выходе, А')
    garantia = models.CharField(max_length=255, verbose_name=u'Гарантия', blank=True)
    toVes = models.ForeignKey(ves, blank=True, null=True, verbose_name=u'Вес')
    toMaterial = models.ForeignKey(material, blank=True, null=True, verbose_name=u'Материал')
    toKoefPuls = models.ForeignKey(koefPuls, blank=True, null=True, verbose_name=u'Коэфициент пульсации')
    toTempDiapazon = models.ForeignKey(tempDiapazon, blank=True, null=True, verbose_name=u'Температурный диапазон')
    toClassDefence = models.ForeignKey(classDefence, blank=True, null=True, verbose_name=u'Класс защиты')
    toIndexColor = models.ForeignKey(indexColor, blank=True, null=True, verbose_name=u'Индекс цветопередачи')
    toSrokSlujbi = models.ForeignKey(srokSlujbi, blank=True, null=True, verbose_name=u'Срок службы')
    toTipKrivoi = models.ForeignKey(tipKrivoi, blank=True, null=True, verbose_name=u'Тип кривой ксс')

    params = models.TextField(verbose_name=u'Параметры', blank=True)
    resetParam = models.BooleanField(verbose_name=u'Сбросить параметры', default=False)

    class Meta:
        db_table = 'products'
        verbose_name = u'Товар'
        verbose_name_plural = u'Товары'

    def get_list_params(self):
        mainList = [[u'Артикул', self.articul],
                    [u'Производитель', self.toProduction],
                    [u'Страна производства', self.toCountry],
                    [u'Гарантия', self.garantia],]
        sortParam = sorted([[u'Цоколь', self.toCokol],
                    [u'Мощность, Вт', self.toWt],
                    [u'Ток, А', self.toTok],
                    [u'Напряжение, В', self.toVolts],
                    [u'Цветовая температура', self.toColorTempr],
                    [u'Световой поток, Lm', self.toSvetPotok],
                    [u'Выходная мощность', self.toOutWt],
                    [u'Цвет свечения, Lm', self.toColorSvechenia],
                    [u'Мощность, 1м/Вт', self.toWtOneM],
                    [u'Размеры, мм', self.toSizes],
                    [u'Цвет', self.toColors],
                    [u'Комплект', self.toComplects],
                    [u'Количество, шт/м', self.toCountShM],
                    [u'Выходное напряжение, В', self.toOutVolts],
                    [u'Управление', self.toUpravlenie],
                    [u'Степень защиты', self.toSecurityStep],
                    [u'Вес', self.toVes],
                    [u'Класс защиты', self.toClassDefence],
                    [u'Коэфициент пульсации', self.toKoefPuls],
                    [u'Материал', self.toMaterial],
                    [u'Температурный диапазон', self.toTempDiapazon],
                    [u'Индекс цветопередачи', self.toIndexColor],
                    [u'Срок службы, ч', self.toSrokSlujbi],
                    [u'Тип кривой ксс', self.toTipKrivoi],
                    [u'Ток на выходе, А', self.toForсeAmperOut],])
        return mainList+sortParam

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    def sale_price(self):
        if self.oldPrice > self.price:
            return self.price
        else:
            return None

    def sale(self):
        return self.oldPrice - self.price

    @models.permalink
    def get_absolute_url(self):
        return ('catalogProduct', (), {'product_slug': self.slug})

class imageForProduct(models.Model):
    toProduct = models.ForeignKey(productModel, verbose_name=u'Товар', related_name='image_product')
    image = models.ImageField(upload_to='images/product/', verbose_name=u"Изображение")

    class Meta:
        db_table = 'imagesProducts'
        verbose_name = u'Изображение'
        verbose_name_plural = u'Изображения'

    def __unicode__(self):
        return 'Изображение: %s' % (self.id)

    def __str__(self):
        return 'Изображение: %s' % (self.id)