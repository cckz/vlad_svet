# -*- coding: utf-8 -*-
from django.contrib import admin
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static

urlpatterns = [
    url(r'^catalog/', include('catalog.urls')),
    url(r'^sender/', include('sender.urls')),
    url(r'^account/', include('account.urls')),
    url(r'^works/', include('ourwork.urls')),
    url(r'^blog/', include('blog.urls')),
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^', include('main.urls')),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)