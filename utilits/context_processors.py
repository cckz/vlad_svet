#-*- coding: utf-8 -*-
from sender.forms import senderForm
from account.forms import authLoginForm
from account.forms import userCreationForm
from main.models import contactsModel
from catalog.models import categoryModel
from main.models import globalSEO

def svetContext(request):
    getCatalog = categoryModel.objects.filter(isActive=True)
    if not request.user.is_authenticated():
        formLogin = authLoginForm()
        formRegister = userCreationForm(prefix=u'reg')
    else:
        formLogin = None
        formRegister = None
    form = senderForm()
    try:
        contacts = contactsModel.objects.filter(isActive=True)[0]
    except IndexError:
        contacts = None
    try:
        globalSEOs = globalSEO.objects.all()[0]
    except IndexError:
        globalSEOs = None
    return  {'form': form,
             'formLogin': formLogin,
             'formRegister': formRegister,
             'contacts': contacts,
             'getCatalog': getCatalog,
             'globalSeo': globalSEOs,
             'request': request}
