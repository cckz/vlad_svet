# -*- coding: utf-8 -*-
from catalog.models import productModel
from catalog.models import categoryModel
from catalog.models import categoryClassModel
from ourwork.models import ourWorksModel
from main.models import certificate
from blog.models import blogModel
from django.contrib.sitemaps import Sitemap
from django.contrib.sites.models import Site
from django.contrib.flatpages.models import FlatPage

class ourWorksSitemap(Sitemap):
    priority = 0.5

    def location(self, item):
        url = '/works/%s/' % (item.slug)
        return url

    def items(self):
        return ourWorksModel.objects.all()

    def get_urls(self, site=None, **kwargs):
        site = Site(domain='lumen35.ru', name='lumen35.ru')
        return super(ourWorksSitemap, self).get_urls(site=site, **kwargs)

class certificateSitemap(Sitemap):
    priority = 0.5

    def location(self, item):
        url = '/certificate/'
        return url

    def items(self):
        return certificate.objects.all()

    def get_urls(self, site=None, **kwargs):
        site = Site(domain='lumen35.ru', name='lumen35.ru')
        return super(certificateSitemap, self).get_urls(site=site, **kwargs)

class blogSitemap(Sitemap):
    priority = 0.5

    def location(self, item):
        url = '/blog/%s/' % (item.slug)
        return url

    def items(self):
        return blogModel.objects.all()

    def get_urls(self, site=None, **kwargs):
        site = Site(domain='lumen35.ru', name='lumen35.ru')
        return super(blogSitemap, self).get_urls(site=site, **kwargs)

class productSitemap(Sitemap):
    priority = 0.5

    def location(self, item):
        url = '/catalog/product/%s/' % (item.slug)
        return url

    def items(self):
        return productModel.objects.all()

    def get_urls(self, site=None, **kwargs):
        site = Site(domain='lumen35.ru', name='lumen35.ru')
        return super(productSitemap, self).get_urls(site=site, **kwargs)

class categorySitemap(Sitemap):
    priority = 0.5

    def location(self, item):
        url = '/catalog/%s/' % (item.slug)
        return url

    def items(self):
        return categoryModel.objects.all()

    def get_urls(self, site=None, **kwargs):
        site = Site(domain='lumen35.ru', name='lumen35.ru')
        return super(categorySitemap, self).get_urls(site=site, **kwargs)

class categoryClassSitemap(Sitemap):
    priority = 0.5

    def location(self, item):
        url = '/catalog/category/%s/' % (item.slug)
        return url

    def items(self):
        return categoryClassModel.objects.all()

    def get_urls(self, site=None, **kwargs):
        site = Site(domain='lumen35.ru', name='lumen35.ru')
        return super(categoryClassSitemap, self).get_urls(site=site, **kwargs)

class flatSitemap(Sitemap):
    priority = 0.5

    def items(self):
        return FlatPage.objects.all()

    def get_urls(self, site=None, **kwargs):
        site = Site(domain='lumen35.ru', name='lumen35.ru')
        return super(flatSitemap, self).get_urls(site=site, **kwargs)