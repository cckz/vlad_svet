# -*- coding: utf-8 -*-
from django.contrib import admin
from sender.models import senderModel

class senderAdmin(admin.ModelAdmin):
    list_display = ('name', 'telephone', 'is_sended', 'created_at')
    list_per_page = 50
    ordering = ['-created_at']
    readonly_fields = ['info']

admin.site.register(senderModel, senderAdmin)

