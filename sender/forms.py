# -*- coding: utf-8 -*-
from django import forms
from sender.models import senderModel

class senderForm(forms.ModelForm):
    class Meta:
        model = senderModel
        fields = "__all__"
        
    def __init__(self, *args, **kwargs):
        super(senderForm, self).__init__(*args, **kwargs)
        self.fields['telephone'].widget.attrs.update({'placeholder': 'Телефон', 'required':'True', 'pattern': '^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{6,10}$'})
        self.fields['telephone'].widget.attrs['class'] = 'input-text'
        self.fields['name'].widget.attrs.update({'placeholder': 'Имя', 'pattern': '^[a-zA-Zа-яА-Я\s-]{2,}$', 'required':'True'})
        self.fields['name'].widget.attrs['class'] = 'input-text'
        self.fields['info'].widget = forms.HiddenInput()