# -*- coding: utf-8 -*-
from sender.views import senderMail
from django.conf.urls import url

urlpatterns = [
    url(r'^$', senderMail, name='senderMail'),
]

