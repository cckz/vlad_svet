# -*- coding: utf-8 -*-
import threading
from django.http import HttpResponse
from django.core.mail import send_mail
from django.views.decorators.csrf import csrf_protect
from django.contrib.sites.shortcuts import get_current_site

from sender.models import senderModel
from sender.forms import senderForm
from catalog.models import productModel
from svet_vlad.settings import EMAIL_HOST_USER

class EmailThread(threading.Thread):
    def __init__(self, subject, content, recipient):
        self.subject = subject
        self.recipient = recipient
        self.content = content
        threading.Thread.__init__(self)

    def run (self):
        send_mail(self.subject, self.content, EMAIL_HOST_USER, self.recipient, fail_silently=False)

@csrf_protect
def senderMail(request):
    if request.method == 'POST' and request.is_ajax():
        postdata = request.POST.copy()
        if 'info' in postdata:
            product = productModel.objects.get(slug=postdata['info'])
            postdata['info'] = 'Наименование: %s\nАртикул: %s\nСсылка: http://%s%s' % (product.name, product.articul,
                                                                                get_current_site(request), product.get_absolute_url())
        form = senderForm(postdata)
        if form.is_valid():
            form.save()

            name_temp = form.cleaned_data['name']
            telephone_temp = form.cleaned_data['telephone']
            info_temp = form.cleaned_data['info']
            #name = str(name_temp.encode('utf-8'))
            #telephone = str(telephone_temp.encode('utf-8'))

            recipients = [EMAIL_HOST_USER, ]
            subj = 'Сообщение с сайта от %s' % (name_temp)
            content = 'Имя: %s\nТелефон: %s\n' % (name_temp, telephone_temp)
            if info_temp:
                content = content+info_temp
            result = EmailThread(subj, content, recipients).start()
            send = senderModel.objects.get(telephone=telephone_temp, name=name_temp)
            send.is_sended = True
            send.save()
            return HttpResponse(status=200)
    return HttpResponse(status=500)