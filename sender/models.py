# -*- coding: utf-8 -*-
from django.db import models

class senderModel(models.Model):
    name = models.CharField(max_length=60, verbose_name='Имя')
    telephone = models.CharField(max_length=15, verbose_name='Телефон')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Дата создания")
    is_sended = models.BooleanField(default=False, verbose_name='Уведомление отправлено')
    info = models.TextField(verbose_name=u'Дополнительная информация', blank=True)

    class Meta:
        db_table='feedback'
        verbose_name='Заявки'
        verbose_name_plural='Заявки'

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name