var gulp = require('gulp')
var concat = require('gulp-concat')
var sass = require('gulp-sass')

gulp.task('sass', function () {
    return gulp.src('src/sass/*.scss')
      .pipe(concat('style.scss'))
      .pipe(sass().on('error', sass.logError))
      .pipe(gulp.dest('static/css'));
})

gulp.task('default', function () {
    gulp.watch('src/sass/*.scss', ['sass']);
});
